use crate::app::{App, AppResult};
use crossterm::event::{KeyCode, KeyEvent, KeyModifiers};

/// Handles the key events and updates the state of [`App`].
pub async fn handle_key_events(key_event: KeyEvent, app: &mut App) -> AppResult<()> {
    match key_event.code {
        // Exit application on `ESC` or `q`
        KeyCode::Esc => {
            app.quit();
        },
        KeyCode::Enter => {
            app.search_station().await;
        },
        KeyCode::Right => {
            app.search_departures().await;
        },
        KeyCode::Backspace => {
            app.query.pop();
        },
        KeyCode::Char(value) => {
            app.query.push(value);
        },
        KeyCode::Up => {
            app.up();
        },
        KeyCode::Down => {
            app.down();
        },
        KeyCode::Home => {
            app.item(0);
        },
        KeyCode::End => {
            app.end();
        },
        // Other handlers you could add here.
        _ => {}
    }
    Ok(())
}
