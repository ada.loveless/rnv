use ratatui::backend::CrosstermBackend;
use ratatui::Terminal;
use rnv_tui::app::{App, AppResult};
use rnv_tui::event::{Event, EventHandler};
use rnv_tui::handler::handle_key_events;
use rnv_tui::tui::Tui;
use std::io;

#[tokio::main]
async fn main() -> AppResult<()> {
    // Create an application.
    let mut app = App::new();

    // Initialize the terminal user interface.
    let backend = CrosstermBackend::new(io::stderr());
    let terminal = Terminal::new(backend)?;
    let events = EventHandler::new(250);
    let mut tui = Tui::new(terminal, events);
    tui.init()?;

    // Start the main loop.
    while app.running {
        // Render the user interface.
        tui.draw(&mut app)?;
        // Handle events.
        match tui.events.next().await? {
            Event::Tick => app.tick(),
            Event::Key(key_event) => handle_key_events(key_event, &mut app).await?,
            Event::Mouse(_) => {}
            Event::Resize(_, _) => {}
        }
        match app.events.next().await? {
            rnv_tui::app::AppEvent::Tick => app.tick(),
            rnv_tui::app::AppEvent::StationResult(result) => app.search_result = result,
            rnv_tui::app::AppEvent::DepartureResult(_) => todo!(),
        }
    }

    // Exit the user interface.
    tui.exit()?;
    Ok(())
}
