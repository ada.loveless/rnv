use ratatui::widgets::TableState;
use rnv::{departures::Journey, stations::Station};
use std::error;
use tokio::sync::mpsc;

/// Application result type.
pub type AppResult<T> = std::result::Result<T, Box<dyn error::Error>>;

#[derive(Debug)]
pub enum AppEvent {
    Tick,
    StationResult(Option<Vec<Station>>),
    DepartureResult(Option<Vec<Journey>>),
}

#[derive(Debug)]
pub struct AppEvents {
    /// Event sender channel.
    sender: mpsc::UnboundedSender<AppEvent>,
    /// Event receiver channel.
    receiver: mpsc::UnboundedReceiver<AppEvent>,
}

impl AppEvents {
    pub fn new() -> Self {
        let (sender, receiver) = mpsc::unbounded_channel();
        AppEvents { sender, receiver }
    }
    /// Receive the next event from the handler thread.
    ///
    /// This function will always block the current thread if
    /// there is no data available and it's possible for more data to be sent.
    pub async fn next(&mut self) -> AppResult<AppEvent> {
        self.receiver
            .recv()
            .await
            .ok_or(Box::new(std::io::Error::new(
                std::io::ErrorKind::Other,
                "This is an IO error",
            )))
    }
}

/// Application.
#[derive(Debug)]
pub struct App {
    /// Is the application running?
    pub running: bool,
    pub query: String,
    pub search_result: Option<Vec<Station>>,
    pub station_state: TableState,
    pub departures: Option<Vec<Journey>>,
    pub events: AppEvents,
}

impl Default for App {
    fn default() -> Self {
        Self {
            running: true,
            query: String::new(),
            search_result: None,
            departures: None,
            station_state: TableState::default(),
            events: AppEvents::new(),
        }
    }
}

impl App {
    /// Constructs a new instance of [`App`].
    pub fn new() -> Self {
        Self::default()
    }

    /// Handles the tick event of the terminal.
    pub fn tick(&self) {}

    /// Set running to false to quit the application.
    pub fn quit(&mut self) {
        self.running = false;
    }

    pub fn item(&mut self, i: usize) {
        let new = self.station_state.selected().map(|_| i);
        self.station_state.select(new)
    }

    pub fn end(&mut self) {
        let new = self.search_result.as_ref().map(|x| x.len() - 1);
        self.station_state.select(new)
    }

    pub fn up(&mut self) {
        let new = match self.station_state.selected() {
            Some(0) => Some(0),
            Some(i) => Some(i - 1),
            None => None,
        };
        self.station_state.select(new)
    }

    pub fn down(&mut self) {
        let new = match self.station_state.selected() {
            Some(i) => {
                if i >= self.search_result.as_ref().map_or(0, |x| x.len()) - 1 {
                    Some(i)
                } else {
                    Some(i + 1)
                }
            }
            None => None,
        };
        self.station_state.select(new)
    }

    pub async fn search_station(&mut self) {
        let search_result = rnv::search_station(&self.query)
            .await
            .ok()
            .map(|x| x.into());
        self.events.sender.send(AppEvent::StationResult(search_result)).unwrap();
        self.station_state.select(Some(0));
    }

    pub async fn search_departures(&mut self) {
        self.departures = match self.station_state.selected() {
            Some(i) => match &self.search_result {
                Some(items) => {
                    let station = items[i].id.clone();
                    rnv::get_departure(&station)
                        .await
                        .ok()
                        .map(|result| result.journeys)
                }
                None => None,
            },
            None => None,
        };
    }
}
