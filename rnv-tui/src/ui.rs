use ratatui::{
    layout::{Constraint, Direction, Layout},
    style::{Color, Style, Stylize},
    text::{Span, Text},
    widgets::{
        block::{Position, Title},
        Block, Borders, Paragraph, Row, Table,
    },
    Frame,
};

use crate::app::App;

/// Renders the user interface widgets.
pub fn render(app: &mut App, frame: &mut Frame) {
    // This is where you add new widgets.
    // See the following resources:
    // - https://docs.rs/ratatui/latest/ratatui/widgets/index.html
    // - https://github.com/ratatui-org/ratatui/tree/master/examples

    let search_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default());

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Length(3), Constraint::Min(1)])
        .split(frame.size());

    let inner = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
        .split(chunks[1]);

    let query = Paragraph::new(Text::styled(
        format!("Query: {}", app.query),
        Style::default().fg(Color::Green),
    ))
    .block(search_block);

    frame.render_widget(query, chunks[0]);

    let items = match &app.search_result {
        Some(stations) => {
            let mut items: Vec<Row> = vec![];
            for station in stations {
                let row = Row::new(vec![
                    format!("{} ", station.id).gray(),
                    format!("{} ", station.parent).into(),
                    format!("{}", station.name).blue(),
                ]);
                items.push(row);
            }
            items
        }
        None => {
            vec![]
        }
    };
    let stations_block = Block::default().borders(Borders::ALL);
    let stations = Table::new(
        items,
        [
            Constraint::Length(4),
            Constraint::Percentage(20),
            Constraint::Percentage(80),
        ],
    )
    .block(stations_block)
    .highlight_style(Style::default().bold())
    .highlight_symbol("> ");

    let foot = Title::from(format!("")).position(Position::Bottom);
    let content_block = Block::default().borders(Borders::ALL).title(foot);
    let widths = [
        Constraint::Length(8),
        Constraint::Length(2),
        Constraint::Length(6),
        Constraint::Min(40),
    ];
    let rows = match &app.departures {
        Some(journeys) => {
            let mut rows: Vec<Row> = vec![];
            for journey in journeys {
                let mut departure = match journey.delay() {
                    Some(i) => {
                        if i > 0 {
                            journey.departure().red()
                        } else {
                            journey.departure().green()
                        }
                    }
                    None => journey.departure().into(),
                };
                if journey.canceled {
                    departure = departure.crossed_out();
                };
                let id = match journey.line.color() {
                    Some(rgb) => Span::styled(
                        &journey.line.id,
                        Style::new().bg(Color::Rgb(rgb.0, rgb.1, rgb.2)),
                    ),
                    None => Span::from(&journey.line.id),
                };
                let destination = Span::styled(&journey.destination, Style::new().blue());
                let row = Row::new(vec![
                    departure,
                    Span::from(journey.line.icon()),
                    id,
                    destination,
                ]);
                rows.push(row);
            }
            rows
        }
        None => {
            vec![]
        }
    };
    let table = Table::new(rows, widths)
        .block(content_block)
        .highlight_style(Style::default().bold())
        .highlight_symbol("> ");

    frame.render_widget(table, inner[1]);
    frame.render_stateful_widget(stations, inner[0], &mut app.station_state);
}
