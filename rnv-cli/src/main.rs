use chrono::{DateTime, Local};
use clap::Parser;
use colored::Colorize;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    /// Station to get info for
    #[clap(short, long)]
    id: Option<String>,
    name: Option<String>,
}

struct Rgb {
    r: u8,
    g: u8,
    b: u8,
}

fn get_color(s: &str) -> Option<Rgb> {
    let s = s.replace("#", "");
    if s.len() >= 6 {
        let r = std::primitive::u8::from_str_radix(&s[0..2], 16).unwrap();
        let g = std::primitive::u8::from_str_radix(&s[2..4], 16).unwrap();
        let b = std::primitive::u8::from_str_radix(&s[4..6], 16).unwrap();
        Some(Rgb { r, g, b })
    } else {
        None
    }
}

fn format_line(line: &rnv::departures::Line) -> String {
    let icon = match line.type_.as_str() {
        "Bus" => "🚌",
        "Straßenbahn" => "🚋",
        "Taxi" => "🚕",
        _ => "",
    };
    let color = get_color(&line.icon_color);
    match color {
        Some(c) => format!("{} {:<4}", icon, line.id.on_truecolor(c.r, c.g, c.b)),
        None => format!("{} {:<4}", icon, line.id),
    }
}

fn format_departure_time_single(time: &DateTime<Local>, cancelled: bool) -> String {
    if cancelled {
        format!("{}", time.format("%H:%M").to_string().strikethrough())
    } else {
        format!("{}", time.format("%H:%M"))
    }
}

fn format_departure_time_double(
    real: DateTime<Local>,
    schedule: DateTime<Local>,
    cancelled: bool,
) -> String {
    let difference = real - schedule; // positive is delays
    let difference_str = format!("{:<+3}", difference.num_minutes());
    let first = format_departure_time_single(&real, cancelled);
    if cancelled {
        format!("{}{:<+3}", first, difference_str.strikethrough())
    } else {
        if difference.num_minutes() > 0 {
            format!("{}{:<+3}", first.red(), difference_str.red())
        } else {
            format!("{}{:<+3}", first.green(), difference_str.green())
        }
    }
}

fn format_departure_time(journey: &rnv::departures::Journey) -> String {
    match (journey.realtime_departure, journey.scheduled_departure) {
        (None, None) => String::new(),
        (None, Some(x)) => format_departure_time_single(&x.into(), journey.canceled),
        (Some(x), None) => format_departure_time_single(&x.into(), journey.canceled),
        (Some(real), Some(schedule)) => {
            format_departure_time_double(real.into(), schedule.into(), journey.canceled)
        }
    }
}

fn format_load(load: &rnv::departures::Loads) -> String {
    match load.load_type.as_str() {
        "I" => String::from("●◯◯"),
        "II" => String::from("●●◯"),
        "III" => String::from("●●●"),
        _ => String::from("   "),
    }
}

fn print_departures(departures: rnv::error::Result<rnv::departures::Departure>) {
    match departures {
        Ok(departures) => {
            println!("Departures at {}:", departures.name.bold().blue());
            for journey in departures.journeys {
                let time = format_departure_time(&journey);
                let load = journey.loads.map_or(String::from("   "), |x| format_load(&x));
                let line = format_line(&journey.line);
                println!(
                    "{:<8} {} {} to {}",
                    time,
                    line,
                    load,
                    journey.destination.blue()
                );
            }
        }
        Err(error) => println!("{}", error),
    }
}

fn search_id_first(name: &str) -> Option<String> {
    let result = rnv::blocking::search_station(&name);
    match result {
        Ok(result) => result
            .results
            .map(|x| {
                x.first()
                    .map(|x| x.children.first().map(|x| x.id.clone()))
                    .flatten()
            })
            .flatten(),
        Err(error) => panic!("{}", error),
    }
}

fn main() {
    env_logger::init();
    let args = Args::parse();
    let id = match (args.id, args.name) {
        (None, None) => panic!("Provide a name or id"),
        (None, Some(name)) => match search_id_first(&name) {
            Some(id) => id,
            None => panic!("No result for search {}", name),
        },
        (Some(id), None) => id,
        (Some(id), Some(_)) => id,
    };
    print_departures(rnv::blocking::get_departure(&id));
}
