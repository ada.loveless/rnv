use std::{error::Error, fmt};

#[derive(Debug)]
pub struct RnvError {
    pub content: String,
}

impl fmt::Display for RnvError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Encountered error in API call:\n{}", self.content)
    }
}

impl Error for RnvError {
}

impl RnvError {
    pub fn new(content: String) -> Self {
        Self { content }
    }
}


pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;
