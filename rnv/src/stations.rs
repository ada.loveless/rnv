
use chrono::serde::ts_seconds_option;
use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct StationResult {
    pub id: String,
    pub trias_id: String,
    pub text: String,
    pub value: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct StationSearchResult {
    pub text: Option<String>,
    pub children: Vec<StationResult>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct StationSearch {
    pub results: Option<Vec<StationSearchResult>>,
    pub cached: bool,
    pub lifetime: Option<i64>,
}

#[derive(Debug)]
pub struct Station {
    pub parent: String,
    pub id: String,
    pub name: String,
}

impl Station {
    fn new(parent: &str, id: &str, name: &str) -> Self {
        Station {
            parent: parent.into(),
            id: id.into(),
            name: name.into(),
        }
    }
}

impl From<StationSearch> for Vec<Station> {
    fn from(value: StationSearch) -> Vec<Station> {
        let mut stations: Vec<Station> = vec![];
        for station in value.results.unwrap_or(vec![]) {
            for child in &station.children {
                stations.push(Station::new(&station.text.clone().unwrap_or(String::new()), &child.id, &child.text.clone()))
            };
        };
        stations
    }
}

