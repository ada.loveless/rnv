use std::collections::HashMap;

use chrono::serde::ts_seconds_option;
use chrono::{DateTime, Local, Utc};
use serde::Deserialize;

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Line {
    pub id: String,
    pub title: String,
    pub type_: String,
    pub type_id: u8,
    pub icon_color: String,
    // pub icon_text_color: Option<String>,
    pub icon_text: String,
    // pub icon_outlined: bool,
}

impl Line {
    pub fn color(&self) -> Option<(u8, u8, u8)> {
        let s = self.icon_color.replace("#", "");
        if s.len() >= 6 {
            let r = std::primitive::u8::from_str_radix(&s[0..2], 16).unwrap();
            let g = std::primitive::u8::from_str_radix(&s[2..4], 16).unwrap();
            let b = std::primitive::u8::from_str_radix(&s[4..6], 16).unwrap();
            Some((r, g, b))
        } else {
            None
        }
    }
    pub fn icon(&self) -> String {
        match self.type_.as_str() {
            "Bus" => "🚌",
            "Straßenbahn" => "🚋",
            "Taxi" => "🚕",
            _ => "",
        }
        .into()
    }
}

impl std::fmt::Display for Line {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {:<4}", self.icon(), self.id)
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Loads {
    pub load_type: String,
}

impl std::fmt::Display for Loads {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let load = match self.load_type.as_str() {
            "I" => "●◯◯",
            "II" => "●●◯",
            "III" => "●●●",
            _ => "   ",
        };
        write!(f, "{}", load)?;
        Ok(())
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Journey {
    pub line: Line,
    pub destination: String,
    pub barrier_level: Option<String>,
    pub loads_forecast_type: Option<String>,
    pub loads: Option<Loads>,
    #[serde(with = "ts_seconds_option")]
    pub realtime_departure: Option<DateTime<Utc>>,
    #[serde(with = "ts_seconds_option")]
    pub scheduled_departure: Option<DateTime<Utc>>,
    #[serde(with = "ts_seconds_option")]
    pub departure: Option<DateTime<Utc>>,
    pub difference: i64,
    pub canceled: bool,
}

fn format_departure_time_single(time: &DateTime<Local>, cancelled: bool) -> String {
    if cancelled {
        format!("{}", time.format("%H:%M").to_string())
    } else {
        format!("{}", time.format("%H:%M"))
    }
}

fn format_departure_time_double(
    real: DateTime<Local>,
    schedule: DateTime<Local>,
    cancelled: bool,
) -> String {
    let difference = real - schedule; // positive is delays
    let difference_str = format!("{:<+3}", difference.num_minutes());
    let first = format_departure_time_single(&real, cancelled);
    if cancelled {
        format!("{}{:<+3}", first, difference_str)
    } else {
        if difference.num_minutes() > 0 {
            format!("{}{:<+3}", first, difference_str)
        } else {
            format!("{}{:<+3}", first, difference_str)
        }
    }
}

impl Journey {
    pub fn delay(&self) -> Option<i64> {
        match (self.realtime_departure, self.scheduled_departure) {
            (Some(real), Some(schedule)) => Some((real - schedule).num_minutes()),
            _ => None,
        }
    }
    pub fn departure(&self) -> String {
        match (self.realtime_departure, self.scheduled_departure) {
            (None, None) => String::new(),
            (None, Some(x)) => format_departure_time_single(&x.into(), self.canceled),
            (Some(x), None) => format_departure_time_single(&x.into(), self.canceled),
            (Some(real), Some(schedule)) => {
                format_departure_time_double(real.into(), schedule.into(), self.canceled)
            }
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Departure {
    pub id: String,
    pub name: String,
    pub lines: HashMap<String, Line>,
    pub journeys: Vec<Journey>,
}
