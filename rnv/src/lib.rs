pub mod error;
pub mod stations;
pub mod departures;
pub mod blocking;

use error::{RnvError, Result};

use format_serde_error::SerdeError;
use reqwest::Client;

pub async fn get_departure(station: &str) -> Result<departures::Departure> {
    let client = Client::builder().user_agent("curl/7.87.0").build().unwrap();
    let uri = format!("https://www.rnv-online.de/rest/departure/{}", station);
    let response = client.get(&uri).send().await?;
    match response.status() {
        reqwest::StatusCode::OK => {
            let body = response.text().await?;
            let data: departures::Departure = serde_json::from_str(&body)
                .map_err(|x| SerdeError::new(body.to_string(), x))?;
            Ok(data)
        }
        i => Err(Box::new(RnvError::new(format!("Non-OK status code {:?}:\n {}", i, response.text().await.unwrap())))),
    }
}

pub async fn search_station(station: &str) -> Result<stations::StationSearch> {
    let client = Client::builder().user_agent("curl/7.87.0").build().unwrap();
    let uri = format!("https://www.rnv-online.de/rest/stop?name={}", station);
    log::debug!("Getting from {uri}");
    let response = client.get(&uri).send().await?;
    match response.status() {
        reqwest::StatusCode::OK => {
            let body = response.text().await?;
            let data: stations::StationSearch = serde_json::from_str(&body)
                .map_err(|x| SerdeError::new(body.to_string(), x))?;
            Ok(data)
        }
        i => Err(Box::new(RnvError::new(format!("Non-OK status code {:?}:\n {}", i, response.text().await.unwrap())))),
    }
}
